import java.lang.reflect.Array;
import java.util.Arrays;

public class RotateArray {
    public static void main(String[] args) {

        // System.out.println(Arrays.toString(rotateArrayUsingIntermediateArray(new
        // int[] { 1, 2, 3, 4, 5, 6, 7 },3)));
        // System.out.println(Arrays.toString(rotateArrayUsingArrayCopy(new int[] { 1, 2, 3, 4, 5, 6, 7 }, 3)));
        System.out.println(Arrays.toString(rotateArrayUsingBubbleRotate(new int[] { 1, 2, 3, 4, 5, 6, 7 }, 3)));
    }

    public static int[] rotateArrayUsingIntermediateArray(int[] arr, int start) {
        if (arr == null || start < 0)
            throw new IllegalArgumentException("Illegal argument!");
        int[] arr2 = new int[arr.length];
        for (int i = start + 1; i < arr.length; i++) {
            arr2[i - start - 1] = arr[i];
        }
        for (int i = 0; i <= start; i++) {
            arr2[i + start] = arr[i];
        }
        return arr2;
    }

    public static int[] rotateArrayUsingArrayCopy(int[] arr, int start) {
        if (arr == null || start < 0) 
            throw new IllegalArgumentException("Illegal argument!");
        int[] arr2 = new int[arr.length];
        System.arraycopy(arr, start + 1, arr2, 0, start);
        System.arraycopy(arr, 0, arr2, start, arr.length - start);

        return arr2;
    }

    public static int[] rotateArrayUsingBubbleRotate(int[] arr, int start) {
        if (arr == null || start < 0)
            throw new IllegalArgumentException("Illegal argument!");
        for(int i =0;i<start;i++){
            for(int j=arr.length-1;j>0;j--){
                int temp=arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
            }
        }

        return arr;
    }

}
